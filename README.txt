Nodereference Embed README
--------------------------------------------------

Nodereference Embed provides configurable (pseudo) tokens that can link to other
assets, such as node or user. Currently, it can be inserted and interpreted in
the node's body text. When viewing the node the token is also rendered by its
actual theme output. This functionality is comparable to Wordpress' shortcode
API (http://codex.wordpress.org/Shortcode_API). So, finally what you get with
this module is a Macro code in Drupal's token style.

Basic usage:    [node id=NID]
                [user id=UID]
Advanced usage: [node id=NID type=[default|link] arg1=XXX arg2="XX Y"]
                [user id=UID type=[default|link] arg1=XXX arg2="XX Y"]

The "id" - as the only mandatory parameter - has to be replaced with the actual
asset id, respectively node or user id. Another fixed, but optional parameter is
"type" which accepts two styles out of the box: "default" renders the node title
and "link" a node link. You can add styles with custom plugins, see "Writing your
own plugin". Optionally, an arbitrary number of arguments can be passed after the
id property. These options will have to be interpreted by your custom plugin.

In admin/settings/nodereference_embed you can configure a default type letting
you omit the definition of the type parameter in the token. However, you can
always override the default setting by defining type in the token manually.


IMPORTANT NOTES
-----------------
From a data view the raw token is saved in the database. Disabling this module
won't process any clean-up work of previously stored tokens. However, you can
get away with configuring a custom input format.

Be aware that Drupal tokens are not dynamic. Therefore this module does some
additional processing. However, tokens are internally recognized as
[node reference embed:node] or [node reference embed:user] without any arguments.


Writing your own plugin
-----------------

Your custom module needs to implements the hook hook_nodereference_embed_plugins():

function nodereference_embed_nodereference_embed_plugins() {
  return array(
    // 1st level: plugin types
    'node' => array(
      // 2nd level: actual plugin handlers
      'default' => array(
        'title' => t('Title (no link)'),
        'handler' => 'nodereference_embed_plugin_node',
        'path' => 'plugins',
      ),
      'link' => array(
        'title' => t('Title (link)'),
        'handler' => 'nodereference_embed_plugin_node_link',
        'parent' => 'nodereference_embed_plugin_node',
        'path' => 'plugins',
      ),
    ),
  );
}

The "handler" and "parent" have to be a sub classes of nodereference_embed_plugin.
A simple example of a plugin is nodereference_embed_plugin_node.inc (see
nodereference_embed_node sub module). Please have a look at these classes for
further details.


Known issues
-----------------
- Not compatible with aggressive or external caching, custom cache invalidation logic required


TODO
-----------------
- WYSWIG editor integration
- Advanced plugins, e. g. views, panels, CCK etc