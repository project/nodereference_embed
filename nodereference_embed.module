<?php
/**
 * @file nodereference_embed.module
 * Inserts a nodereference as a token into the node's body text.
 * The token is replaced with its rendered (HTML) output before the page is displayed.
 */

/**
 * We replace our custom placeholder before the node is rendered ($op = 'alter').
 * Currently we interpret tokens in the node's body text.
 *
 * Implementation of hook_nodeapi()
 */
function nodereference_embed_nodeapi(&$node, $op, $a3 = null, $a4 = null) {
  if ($op == 'alter') {
    nodereference_embed_process_tokens($node->body);
  }
}

/**
 * Implementation of hook_init().
 * Include panes configuration and display if modules are activated.
 */
function nodereference_embed_init() {
  if (module_exists('ctools') && module_exists('panels')) {
    module_load_include('inc', 'nodereference_embed', 'includes/panels');
  }
}

/**
 * We search the text for matching tokens and replace it with their themed HTML representations.
 *
 * @param $text
 *   The text fragment to be searched for an embed token
 * @param $display
 *   default display type as specified in settings
 */
function nodereference_embed_process_tokens(&$text, $display = 'default') {

  module_load_include('inc', 'nodereference_embed', 'includes/plugins'); // load plugin handling file
  $plugin_types = array_keys(nodereference_embed_discover_plugins());

  if (!empty($plugin_types)) {
    foreach ($plugin_types as $plugin_type) {
      // search for all nodereference embed tokens in provided text
      if (preg_match_all("/\[\s*(node reference embed:$plugin_type|$plugin_type)\s+id=[\"|']?(\d+)[\"|']?\s*[\||\s]?\s*([^\]]*)\s*\]/", $text, $matches)) {
        for ($i = 0; $i < count($matches[0]); $i++) {
          if (!empty($matches[1][$i]) && !empty($matches[2][$i])) {
            $options = array();
            $options['type'] = $display; // set default display

            // parse token arguments into options array
            $matches[3][$i] = htmlspecialchars_decode($matches[3][$i]); // decode ampersands & if used (WYSIWIG)
            if (preg_match_all('/\s*&?([^=\s&]*)\s*=\s*("[^="&]*"|[^="\s&]*)\s*/', $matches[3][$i], $sub_matches)) {
              foreach ($sub_matches[2] as &$sub_match) { // strip quotes
                $sub_match = str_replace('"', '', $sub_match);
              }
              $options = array_merge($options, array_combine($sub_matches[1], $sub_matches[2]));
            }

            $id = $matches[2][$i]; // node id
            $text = str_replace($matches[0][$i], '[' . $matches[1][$i] .']', $text);
            // replace token with actual value
            $text = token_replace($text, 'node reference embed', $id, TOKEN_PREFIX, TOKEN_SUFFIX, $options);
          }
        }
      }
    }
  }
}

/**
 * Implementation of hook_token_list()
 */
function nodereference_embed_token_list($type = 'all') {
  if ($type == 'node reference embed' || $type == 'all') {
    $tokens = array();

    module_load_include('inc', 'nodereference_embed', 'includes/plugins'); // load plugin handling file
    foreach (nodereference_embed_discover_plugins() as $plugin_type => $info) {
      $tokens['node reference embed'][$plugin_type] = t("Embeds a rendered {$plugin_type}: Property id expects the {$plugin_type} id (mandatory) and default for type=\"" . variable_get("nodereference_embed_plugin_{$plugin_type}_default_type", 'default') . '"');
      $tokens['node reference embed'][$plugin_type] .= '<ul>';
      foreach ($info as $plugin => $handler) {
        $tokens['node reference embed'][$plugin_type] .= '<li>' . $handler->help() . '</li>';
      }
      $tokens['node reference embed'][$plugin_type] .= '</ul>';
    }
    return $tokens;
  }
}

/**
 * The placeholder in hook_token_list() are replaced by the here provided values on runtime.
 *
 * Implementation of hook_token_values()
 */
function nodereference_embed_token_values($type, $object = NULL, $options = array()) {
  if ($type == 'node reference embed' && intval($object)) {
    $id = intval($object);

   // Process the proper theme function encapsulated in a plugin
   $plugin_types = array_keys(nodereference_embed_discover_plugins());
   foreach ($plugin_types as $plugin_type) {
     $values[$plugin_type] = nodereference_embed_process_plugin($plugin_type, $id, $options);
   }

   return $values;
  }
}

/**
 * Process the Embed plugin, i. e. load the proper plugin class and theme the token.
 *
 * @param $type
 *   plugin type, e. g. "node"
 * @param $id
 *   identified, e. g. node id
 * @param $options
 *   optional arguments array for theming
 * @return
 *   HTML string output
 */
function nodereference_embed_process_plugin($plugin_type, $id, $options = array()) {
  // Load all available plugins
  $plugins = nodereference_embed_discover_plugins($plugin_type);

  // Object factory: get the default plugin (sub class), first user input, then configuration and as a fallback the default value
  $type = $options['type'] ? $options['type'] : variable_get("nodereference_embed_plugin_{$plugin_type}_default_type", 'default');

  $plugin = $plugins[$type]; // get plugin with proper display
  if (is_object($plugin) && is_subclass_of($plugin, 'nodereference_embed_plugin')) {
    $plugin->load($id); // Load data
    return $plugin->render($options); // Theme output
  }
}

/**
 * Appends the tokens provided by this module after the body text in the node edit form.
 *
 * Implementation of hook_form_alter().
 */
function nodereference_embed_form_alter(&$form, $form_state, $form_id) {
  if (isset($form['#node']) && $form['#node']->type .'_node_form' == $form_id) {
    $form['body_field']['embed_tokens'] = array(
      '#type' => 'fieldset',
      '#title' => t('Embed tokens'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );

    $form['body_field']['embed_tokens']['nr_embed'] = array(
      '#type' => 'markup',
      '#value' => theme('token_tree', array('node reference embed'), FALSE),
    );
  }
}

/**
 *
 *
 * Implementation of hook_menu().
 */
function nodereference_embed_menu() {
  return array(
    'admin/settings/nodereference_embed' => array(
      'access arguments' => 'administer node reference embed',
      'title' => t('Nodereference Embed Settings'),
      'page callback' => 'drupal_get_form',
      'page arguments' => array('nodereference_embed_settings'),
      'type' => MENU_CALLBACK,
      'file' => 'admin.inc',
      'file path' => drupal_get_path('module', 'nodereference_embed') . '/includes',
    ),
  );
}