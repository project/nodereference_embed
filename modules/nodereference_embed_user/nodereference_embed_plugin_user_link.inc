<?php
/**
 * @file nodereference_embed_plugin_node_link.inc
 * Render the token as a node link
 */

class nodereference_embed_plugin_user_link extends nodereference_embed_plugin_user {

  /**
   * Provides a token help text.
   */
  public function help() {
    return t($this->title . ': [user id=ID type="link"]');
  }

  /**
   * Render a link to the node.
   */
  public function render($options = array()) {
    return l($this->user->name ? $this->user->name : drupal_get_path_alias('user/' . $this->user->uid), 'user/' . $this->user->uid);
  }
}