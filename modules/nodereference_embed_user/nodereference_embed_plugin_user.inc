<?php
/**
 * @file nodereference_embed_plugin_user.inc
 * Base class for all user displays. Defines default display in settings() method.
 */

class nodereference_embed_plugin_user extends nodereference_embed_plugin {

  protected $user = NULL;

  /**
   * Load the node, override in subclass for custom queries/views.
   */
  public function load($id) {
    $this->user = user_load($id);
  }

  /**
   * Provides a token help text.
   */
  public function help() {
    return t($this->title . ': [user id=ID type="default"]');
  }

  /**
   * Provide a configurable node display (subclass) to be loaded automatically.
   * TODO: Move into content type settings.
   */
  public function settings() {
    // get all displays of the node plugin
    $plugins = nodereference_embed_discover_plugins('user');
    foreach ($plugins as $key => $plugin) {
      $options[$key] = $plugin->title;
    }
    arsort($options); // sort array

    $form['nodereference_embed_plugin_user_default_type'] = array(
      '#type' => 'select',
      '#title' => t('Default user display'),
      '#options' => $options,
      '#default_value' => variable_get('nodereference_embed_plugin_user_default_type', 'default'),
    );
    return $form;
  }

  /**
   * By default display the node title only
   */
  public function render($options = array()) {
    return $this->user->name;
  }
}