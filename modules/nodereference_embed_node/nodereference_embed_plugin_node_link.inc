<?php
/**
 * @file nodereference_embed_plugin_node_link.inc
 * Render the token as a node link
 */

class nodereference_embed_plugin_node_link extends nodereference_embed_plugin_node {

  /**
   * Provides a token help text.
   */
  public function help() {
    return t($this->title . ': [node id=NID type="link"]');
  }

  /**
   * Render a link to the node.
   */
  public function render($options = array()) {
    return l($this->node->title ? $this->node->title  : drupal_get_path_alias('node/' . $this->node->nid), 'node/' . $this->node->nid);
  }
}