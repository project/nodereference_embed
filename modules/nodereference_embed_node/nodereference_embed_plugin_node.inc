<?php
/**
 * @file nodereference_embed_plugin_node.inc
 * Base class for all node displays. Defines default display in settings() method.
 */

class nodereference_embed_plugin_node extends nodereference_embed_plugin {

  protected $node = NULL;

  /**
   * Load the node, override in subclass for custom queries/views.
   */
  public function load($id) {
    $this->node = node_load($id);
  }

  /**
   * Provides a token help text.
   */
  public function help() {
    return t($this->title . ': [node id=NID type="default"]');
  }

  /**
   * Provide a configurable node display (subclass) to be loaded automatically.
   * TODO: Move into content type settings.
   */
  public function settings() {
    // get all displays of the node plugin
    $plugins = nodereference_embed_discover_plugins('node');
    foreach ($plugins as $key => $plugin) {
      $options[$key] = $plugin->title;
    }
    arsort($options); // sort array

    $form['nodereference_embed_plugin_node_default_type'] = array(
      '#type' => 'select',
      '#title' => t('Default node display'),
      '#options' => $options,
      '#default_value' => variable_get('nodereference_embed_plugin_node_default_type', 'default'),
    );
    return $form;
  }

  /**
   * By default display the node title only
   */
  public function render($options = array()) {
    return $this->node->title;
  }
}