<?php
/**
 * @file plugins.inc
 * Contains all plugin handling logic
 */

/**
 * Load all plugin object by collecting all modules implementing the plugin info hook.
 * @return
 *   array of hook info
 */
function nodereference_embed_discover_plugins($type = 'all') {
  static $plugins = array();
  if (empty($plugins)) {
    foreach (module_implements('nodereference_embed_plugins') as $module) {
      $function = $module . '_nodereference_embed_plugins';
      $result = call_user_func($function);

      foreach ($result as $plugin_type => $info) {
        foreach ($info as $plugin => $def) {
          // load parent file first, if file exists directly,
          // otherwise with Drupal API
          $parent_inc = $def['parent'];
          _nodereference_embed_load_include($parent_inc, $module);

          // load main class
          $handler_inc = $def['handler']; // name of handler class
          if ($def['path']) { // append path to handler class
            $handler_inc = $def['path'] . '/' . $handler_inc;
          }
          _nodereference_embed_load_include($handler_inc, $module);

          // store and initiate plugin object if plugin class exists
          if (class_exists($def['handler'])) {
            if ($def['title']) { // title is important for settings page
              $plugins[$plugin_type][$plugin] = new $def['handler']($def['title']);
            }
            else {
              $plugins[$plugin_type][$plugin] = new $def['handler']($plugin);
            }
          }
        }
      }
    }
  }
  return $type == 'all' ? $plugins : $plugins[$type];
}

/**
 * Include a PHP file by name (without extension)
 * @param $file_name
 * @param string $module
 */
function _nodereference_embed_load_include($file_name, $module = 'nodereference_embed') {
  if (file_exists("$file_name.inc")) {
    include_once("$file_name.inc");
  }
  else {
    module_load_include('inc', $module, $file_name);
  }
}

/**
 * Class nodereference_embed_plugin.inc
 * All embed plugins must extend this base class and implements its methods.
 */
abstract class nodereference_embed_plugin {

  public $title;

  /*
   *
   */
  public function __construct($title) {
    $this->title = $title;
  }

  /**
   * Get relevant data by id to be stored in the plugin object temporarily.
   * @param $id
   *   identifer for data source
   */
  public abstract function load($id);


  /**
   * Returns a help text providing a brief description of the token's syntax.
   * @return string
   */
  public abstract function help();

  /*
   * Form array of configuration options which will be stored in variables
   * as system_settings_form() invocation is applied.
   * @return
   *   form array
   */
  public abstract function settings();

  /**
   * Theme the plugin with dynamic options.
   * @param $options
   *   array of theming options
   * @return
   *   rendered HTML representation
   */
  public abstract function render($options = array());

}