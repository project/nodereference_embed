<?php
/**
 * @file nodereference_embed.panels.inc
 * Integration with panels configuration and display.
 */

/**
 * Implementation of hook_ctools_render_alter().
 *
 * As hook_nodeapi($op='alter') is not called if ctools' page manager overrides it,
 * another hook has to massage the body text before rendering.
 */
function nodereference_embed_ctools_render_alter(&$info, $page, $args, $contexts, $task, $subtask, $handler) {
  if ($handler->conf['nodereference_embed_state']) {
    nodereference_embed_process_tokens($info['content'], $handler->conf['nodereference_embed_display']);
  }
}

/**
 * Implements hook_page_manager_variant_operations_alter().
 *
 * Shameful rip off from panels_everywhere module.
 *
 * @see panels_everywhere_page_manager_variant_operations_alter()
 * @see http://groups.drupal.org/node/110719#comment-356094
 */
function nodereference_embed_page_manager_variant_operations_alter(&$operations, $handler) {

  // Use this obnoxious construct to safely insert our item.
  reset($operations['children']);
  $children_operations = array();
  while (list($key, $value) = each($operations['children'])) {
    $children_operations[$key] = $value;
    if ($key == 'content') {
      $children_operations['nodereference_embed'] = array(
        'title' => t('Embedded Content'),
        'description' => t('Edit embedded content settings.'),
        'form' => 'nodereference_embed_variant_nr_embed_form',
      );
    }
  }
  $operations['children'] = $children_operations;
}

/**
 * Custom form settings for embedded content.
 */
function nodereference_embed_variant_nr_embed_form(&$form, &$form_state) {

  ctools_include('context-task-handler');

  $handler = &$form_state['handler'];
  $contexts = ctools_context_handler_get_all_contexts($form_state['task'], $form_state['subtask'], $handler);

  if (empty($handler->conf['nodereference_embed_state'])) {
    $handler->conf['nodereference_embed_state'] = '0';
  }

  if (empty($handler->conf['nodereference_embed_display'])) {
    $handler->conf['nodereference_embed_display'] = '0';
  }

  $form['settings']['nodereference_embed_state'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable embedded content for this Variant'),
    '#default_value' => $handler->conf['nodereference_embed_state'],
  );

  module_load_include('inc', 'nodereference_embed', 'includes/plugins'); // load plugin handling file
  $plugins = nodereference_embed_discover_plugins('node');
  foreach ($plugins as $key => $plugin) {
    $options[$key] = $plugin->title;
  }
  arsort($options); // sort array

  $form['settings']['nodereference_embed_display'] = array(
    '#type' => 'select',
    '#title' => t('Default display'),
    '#options' => $options,
    '#default_value' => $handler->conf['nodereference_embed_display'],
  );

  $rows = array();
  foreach ($contexts as $context) {
    foreach (ctools_context_get_converters('%' . check_plain($context->keyword) . ':', $context) as $keyword => $title) {
      $rows[] = array(
        check_plain($keyword),
        t('@identifier: @title', array('@title' => $title, '@identifier' => $context->identifier)),
      );
    }
  }
}

/**
 * Handles Panels nodereference embed form submission.
 */
function nodereference_embed_variant_nr_embed_form_submit(&$form, &$form_state) {
  $form_state['handler']->conf['nodereference_embed_state'] = $form_state['values']['nodereference_embed_state'];
  $form_state['handler']->conf['nodereference_embed_display'] = $form_state['values']['nodereference_embed_display'];
}