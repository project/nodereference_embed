<?php
/**
 * @file admin.inc
 * Configuration/administration settings.
 */

/* Administration settings form.
 * TODO: Move into content type settings.
 * @return array
 *   form array
 */
function nodereference_embed_settings() {
  $form = array();

  module_load_include('inc', 'nodereference_embed', 'includes/plugins'); // load plugin handling file
  // load all settings form of all plugins into a single form
  $plugins = nodereference_embed_discover_plugins();
  foreach ($plugins as $plugin_type => $displays) {
    foreach ($displays as $display) {
      $form += $display->settings($displays); // add form
    }
  }
  return system_settings_form($form);
}